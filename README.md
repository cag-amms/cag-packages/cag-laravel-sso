# CAG SSO
Single Sign On for CAG

## Installation
You can install the package via composer:

```shell
composer require ndoptor/integration-sso-laravel
```

### Laravel 7.x and above
The package will automatically register itself, so you can start using it immediately.

## Configuration

#### After installing the package, need to update or add these lines on `.env` file
- `LOGIN_SSO_URL=http://sso-url.test/login`
- `LOGOUT_SSO_URL=http://sso-url.test/logout`

#### Update`web.php` in `routes` directory
- Remove `Auth::routes();`, if exists.

#### Use `cag.sso` for CAG SSO authentication
```
Route::middleware(['cag.sso'])->group(function () {
    /// here your authentication route
});
```

Thank you for using it.
