<?php

namespace Cag\CagSSO;

use Cag\CagSSO\Http\Middleware\CagAuthenticate;
use Illuminate\Contracts\Http\Kernel;
use Illuminate\Routing\Router;
use Illuminate\Support\ServiceProvider;

class CagSSOServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadRoutesFrom(__DIR__ . '/routes/web.php');

        $router = $this->app->make(Router::class);
        $router->aliasMiddleware('cag.sso', CagAuthenticate::class);
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfigFrom(
            __DIR__ . '/config/cag.php', 'cag'
        );
    }
}
