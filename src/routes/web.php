<?php

/*
|--------------------------------------------------------------------------
| Crud Generator Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/login', 'Cag\CagSSO\Http\Controllers\CagSSOController@showLoginForm')->name('login');
Route::get('/login-response', 'Cag\CagSSO\Http\Controllers\CagSSOController@loginResponse')->name('login-response')->middleware('web');
Route::post('/logout', 'Cag\CagSSO\Http\Controllers\CagSSOController@logout')->name('logout')->middleware('web');